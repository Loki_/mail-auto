package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Loki_ on 22-Sep-16.
 */
public class MailPage extends AbstractPage {

    // [ This Inbox is currently Empty ]
    @FindBy (id = "public_count")
    private WebElement mailCount;

    // Logout
    @FindBy (xpath="//div/ul/li[text()='Logout']")
    private WebElement logout;

    public MailPage (WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public int getMailCount() {
        int mailBox = Integer.parseInt(mailCount.getText());
        return mailBox;
    }
}
