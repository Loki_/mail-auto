package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Loki_ on 22-Sep-16.
 */
public class LoginPage extends AbstractPage {
    private String URL = "https://www.mailinator.com/login.jsp";

    // Login
    @FindBy (id="loginEmail")
    private WebElement userName;

    // Password
    @FindBy (id="loginPassword")
    private WebElement password;

    //Submit
    @FindBy (xpath="//button[contains(@class, 'btn-lg')]")
    private WebElement loginButton;

    public LoginPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    // Method for get URL for login page
    public LoginPage openUrl() {
        getDriver().get(URL);
        return this;
    }

    // Method for login
    public LoginPage makeLogin(String login, String pass) {
        this.openUrl();
        userName.clear();
        userName.sendKeys(login);
        password.clear();
        password.sendKeys(pass);
        loginButton.click();
        return this;
    }

}
