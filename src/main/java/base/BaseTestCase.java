package base;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import pages.LoginPage;
import pages.MailPage;

import java.io.IOException;

/**
 * Created by Loki_ on 22-Sep-16.
 */
public class BaseTestCase extends BaseTest {
    public LoginPage loginPage;
    public MailPage mailPage;

    @BeforeMethod
    public void getPage() {
        loginPage = new LoginPage(driver);
        PageFactory.initElements(getDriver(), LoginPage.class);
        mailPage = new MailPage(driver);
        PageFactory.initElements(getDriver(), MailPage.class);
    }
    @BeforeClass
    public void setUp() throws IOException {
        chooseBrowsers();
    }
    @BeforeTest
    public String getLogin() {
        String login = null;
        for (int i = 0; i < 3; i++) {
            login = ChooseUser.getUsers().getProperty("login" + (i + 1));
        }
        return login;
    }
    public String getPass() {
        String pass = null;
        for (int i = 0; i < 3; i++) {
            pass = ChooseUser.getUsers().getProperty("pass" + (i + 1));
        }
        return pass;
    }
}
