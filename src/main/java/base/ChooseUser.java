package base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by Loki_ on 05.10.16.
 */
public final class ChooseUser {
    private static ChooseUser users = null;

    private static Properties props = new Properties();
    private static String sDirSeparator = System.getProperty("file.separator");

    private ChooseUser() {
        try {
            FileInputStream fis = new FileInputStream(new File(".").getCanonicalPath() + sDirSeparator + "users.properties");
            props.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ChooseUser getUsers() {
        if (users == null) {
            users = new ChooseUser(); }
        return users;
    }
    // Get the value of setting by name
    public String getProperty(String key) {
        String value = null;
        if(props.containsKey(key))
            value = (String) props.get(key);
        else {
            System.out.println("The property is not found");
        }
        return value;
    }
}
