import base.BaseTestCase;
import base.ChooseUser;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.MailPage;

import java.io.IOException;

/**
 * Created by Loki_ on 22-Sep-16.
 */
public class LoginTest extends BaseTestCase {
    private static int expectedResult = 0;

    @Test
    public void loginTest() throws IOException {
        LoginPage logPass = new LoginPage(getDriver());

            logPass.makeLogin(getLogin(), getPass());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            MailPage inb = new MailPage(driver);
            int actualResult = inb.getMailCount();
            //System.out.println(actualResult);
            Assert.assertNotEquals(actualResult, expectedResult, "The E-mail has no letters");
    }
}
